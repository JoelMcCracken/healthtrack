{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
, name =
    "my-project"
, dependencies =
    [ "effect"
    , "console"
    , "react"
    , "react-basic"
    , "react-basic-classic"
    , "react-basic-native"
    , "typelevel-prelude"
    , "debug"
    , "simple-json"
    , "foreign"
    , "foreign-generic"
    , "generics-rep"
    , "now"
    , "undefined"
    , "formatters"
    , "js-date"
    , "simple-moment"
    , "debugged"
    , "undefinable"
    , "aff"
    ]
, packages =
    ./packages.dhall
}
